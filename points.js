#!/usr/bin/nodejs

var colors = require("colors");
var fs = require("fs");
var hjson = require("hjson");
var prompt = require("prompt-sync")();
var sprintf = require('sprintf-js').sprintf;
var dateFormat = require('dateformat');

var argv = require("yargs")
.usage("Usage: points [options] [student]")

.alias("n", "new")
.boolean("new")
.describe("new", "Add a new student to the system.")
.group("new", "Commands")

.alias("l", "late")
.boolean("late")
.describe("late", "Note that a student is late.")
.group("late", "Commands")

.alias("r", "result")
.boolean("result")
.describe("result", "Add a result to student.")
.group("result", "Commands")

.alias("p", "project")
.boolean("project")
.describe("project", "Give point to student for project work.")
.group("project", "Commands")

.alias("t", "task")
.boolean("task")
.describe("task", "Create a task for a student and subject.")
.group("task", "Commands")

.alias("T", "print_task")
.boolean("print_task")
.describe("print_task", "Print all active Task in order.")
.group("print_task", "Commands")

.alias("u", "prioritise")
.nargs("prioritise",1)
.describe("prioritise", "Prioritise up a task.")
.group("prioritise", "Commands")

.alias("U", "downPrioritise")
.nargs("downPrioritise",1)
.describe("downPrioritise", "Prioritise down a task.")
.group("downPrioritise", "Commands")

.alias("d", "done")
.nargs("done",1)
.describe("done", "Finish a task.")
.group("done", "Commands")

.alias("D", "deadline")
.nargs("deadline",1)
.describe("deadline", "Finish a task.")
.group("deadline", "Commands")

.alias("s", "show")
.boolean("show")
.describe("show", "Show info about a student.")
.group("show", "Commands")

.alias("a", "show-all")
.boolean("show-all")
.describe("show-all", "Show _all_ info about a student.")
.group("show-all", "Commands")

.alias("h", "help")
.help("h")
.group("help", "More info")

.alias("v", "verbose")
.count("verbose")
.describe("verbose", "Print more infromation about what happens.")
.group("verbose", "More info")

.alias("V", "empty")
.count("empty")
.describe("empty", "Do nothing, needed for at bug in option parsing.")
.group("empty", "More info")

.alias("C", "no-colors")
.boolean("no-colors")
.describe("no-colors", "No colors in output, good for copying text.")
.group("no-colors", "More info")

.alias("i", "issue")
.boolean("issue")
.describe("issue", "This command is about a issue on a project.")
.group("issue", "Project type")

.alias("c", "comment")
.boolean("comment")
.describe("comment", "This command is about a comment on a project.")
.group("comment", "Project type")

.alias("k", "commit")
.boolean("commit")
.describe("commit", "This command is about a git commit on a project.")
.group("commit", "Project type")

.alias("b", "bad")
.count("bad")
.describe("bad", "The studen has done something bad.")
.group("bad", "Opinion")

.alias("g", "good")
.count("good")
.describe("good", "The studen has done something good.")
.group("good", "Opinion")

.alias("m", "text")
.nargs('text', 1)
.describe("text", "A comment on the command.")
.group("text", "Opinion")

.alias("1", "prog1")
.boolean("prog1")
.describe("prog1", "The comands is about the subject 'Programmering 1'.")
.group("prog1", "Subject")

.alias("2", "prog2")
.boolean("prog2")
.describe("prog2", "The comands is about the subject 'Programmering 2'.")
.group("prog2", "Subject")

.alias("3", "webserver")
.boolean("webserver")
.describe("webserver", "The comands is about the subject 'Webbserverprogrammering 1'.")
.group("webserver", "Subject")

.alias("4", "electronic")
.boolean("electronic")
.describe("electronic", "The comands is about the subject 'Elektronik och Mikrodatorteknik'.")
.group("electronic", "Subject")

.alias("5", "computer")
.boolean("computer")
.describe("computer", "The comands is about the subject 'Datorteknik 1a'.")
.group("computer", "Subject")

.alias("6", "htsitProg")
.boolean("htsitProg")
.describe("htsitProg", "The comands is about doing programing for HTSIT.")
.group("htsitProg", "Subject")

.epilog("copyright 2016 by Magnus Kronnäs")
.argv;

var out_dir = "data/";
var data;
var alias=argv["_"][0];

var WARN = function() {};
if(argv["verbose"] > 0) {
    WARN = function(msg) {console.log(msg);};
}
WARN("Temp for eslint.");

var INFO = function() {};
if(argv["verbose"] > 1) {
    INFO = function(msg) {console.log(msg);};
}

var DEBUG = function() {};
if(argv["verbose"] > 2) {
    DEBUG = function(msg) {console.log(msg);};
}

DEBUG(argv);

var RED = colors.red;
var GREEN = colors.green;
var BLUE = colors.blue;
var YELLOW = colors.yellow;
var UNDERLINE = colors.underline;
var RESET = colors.reset;
var BOLD=colors.bold;

if(argv["C"]) {
    var NONE = function(msg) {return msg};
    RED = NONE;
    GREEN = NONE;
    BLUE=NONE;
    UNDERLINE = NONE;
    RESET = NONE;
    BOLD=NONE;
    YELLOW=NONE;
}

var printStudentType = function(type) {
    if(type === "o") {
    return "😺";
    } 
    else if(type === "w") {
    return "😫";
    }
    else if(type === "t2") {
    return "♣";
    }
    else if(type === "t3") {
    return "♠";
    }
    
    return "☢";
}

var printLog = function(log) {
	var result="";
	
	result+=GREEN(sprintf("%-16s", log["type"]));
	result+=" "+ BLUE(dateFormat(log["date"], "yyyy-mm-dd HH:MM:ss"));
	
	if (typeof log["result"]  !== "undefined") {
	result+=" "+ sprintf("Karma:  %+3d", log["result"]);
	}
	
	if (typeof log["subject"]  !== "undefined") {
	result+=" "+ sprintf("Sub: %-12s", log["subject"]);
	}
	
	if (typeof log["comment"]  !== "undefined") {
	result+=" "+ sprintf("Com: %-12s", log["comment"]);
	}
	
	return result;
}

var prinTask = function(task, index) {
	var result="";

	if(!task["deadline"]) {
    result+=BOLD(index) +" "+GREEN(task["comment"]) + "\n";
}
else {
	result+="\n"+RED(task["comment"]) + "\n\n";
}
	return result;
}

var prinTasks = function(tasks) {
	var result="";

   for(var i = 0; i < student.tasks.length;i++) {
       result+=prinTask(tasks[i],i); 
   }

	return result;
}

var prinTaskOnSubject = function(tasks,subject) {
	var result="";
   for(var i = 0; i < student.tasks.length;i++) {
	   if("subject" in tasks[i] && tasks[i]["subject"] === subject) {
		result+=prinTask(tasks[i],i);
	} 
   }

	return result;
}	

var printMetaInfoStudent = function(student) {
	var result="";
	
	result+=RED(UNDERLINE(BOLD(student["alias"])));
	result+="\n";
	
	result+=YELLOW(printStudentType("type"));
	result+=" "+ GREEN(student["name"]);
	result+="\n";
	
	result+=BLUE(student["class"]);
	result+=":"+BLUE(student["mentor"]);
	result+="\n";
	
	result+=BOLD(RED(student["karma"]));
	result+="\n";
	
	
	return result;
}

var loadStudent = function(student_alias) {
    data = fs.readFileSync(out_dir+student_alias+".hjson", "utf8");
    DEBUG(data);
    return hjson.parse(data);
}

// Create a log since it may be necessary
var log={};
log["date"] = (new Date()).toJSON();

if(argv["1"]) {
   log["subject"] = "prog1";
}

if(argv["2"]) {
   log["subject"] = "prog2";
}

if(argv["3"]) {
   log["subject"] = "webserver";
}

if(argv["4"]) {
   log["subject"] = "electronic";
}

if(argv["5"]) {
   log["subject"] = "computer";
}

if(argv["6"]) {
   log["subject"] = "htsitProg";
}

if("text" in argv) {
   log["comment"] = argv["text"];
}

if(argv["new"]) {
    var index_filename = "data/index.hjson";

    data = fs.readFileSync(index_filename,"utf8");

    var student_list = hjson.parse(data);
    var student={};

    var student_name = prompt(GREEN("Vad heter eleven?"));
    var student_mentor = prompt(GREEN("Vad har eleven för mentor?"));
    var student_class = prompt(GREEN("Vilken class går eleven i? "));
    var student_type = prompt(GREEN("Vilken grupptillhör eleven? (t2/t3/w/o)"));
    student["name"]= student_name;
    student["mentor"]= student_mentor;
    student["class"]= student_class;
    student["type"]= student_type;
    student["alias"]= alias;
    
    student["tasks"]=[];
    student["done"]=[];
    student["logs"]=[];
    student["result"]=[];
    student["note"]=[];
    student["late"]=[];
    
    student_list[alias]=alias;
    
    student["karma"]= 0;                                                                                                                                                student["logs"]=[];

    fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
    fs.writeFileSync(index_filename, hjson.stringify(student_list), "utf8");
}

if(argv["task"]) {
    student = loadStudent(alias);
    
    log["type"] = "task-started";
    var log_result;
    
    var task = {}
    task["deadline"]=false;
    
    if("subject" in log) {
    task["subject"]=log["subject"];
}

    task["start"]=log["date"];
    
    if(!"comment" in argv) {
        log["comment"] = prompt(GREEN("Vad går uppgiften ut på?"));
    }

    task["comment"]=log["comment"];

    
    student["tasks"].push(task);
    student["logs"].push(log); 
    fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

if(argv["deadline"]) {
    student = loadStudent(alias);
    
    log["type"] = "deadline-add";
    var log_result;
    
    var task = {}
    task["deadline"]=true;
    
    if("subject" in log) {
    task["subject"]=log["subject"];
}

    task["start"]=log["date"];
    
    if(!"comment" in argv) {
        log["comment"] = prompt(GREEN("När är deadlinen?"));
    }
    
    task["comment"]=log["comment"];

    
    student["tasks"].push(task);
    student["logs"].push(log); 
    fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

if("prioritise" in argv) {
    student = loadStudent(alias);
    var temp = student["tasks"][argv["prioritise"]];
    student["tasks"][argv["prioritise"]]=student["tasks"][argv["prioritise"]-1];
    student["tasks"][argv["prioritise"]-1]=temp;
        fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

if("downPrioritise" in argv) {
    student = loadStudent(alias);
    var temp = student["tasks"][argv["downPrioritise"]];
    student["tasks"][argv["downPrioritise"]]=student["tasks"][argv["downPrioritise"]+1];
    student["tasks"][argv["downPrioritise"]+1]=temp;
        fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

if(typeof argv["done"] !== "undefined") {;
    student = loadStudent(alias);
    var task = student["tasks"][argv["done"]];
    log["type"] = "task-done";
    log["comment"] = task["comment"];
    
    if("subject" in task) {
   log["subject"] = task["subject"];
} 
    
    student["logs"].push(log);
    
     student["tasks"].splice(argv["done"],1);

    fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

if(argv["print_task"]) {
    student = loadStudent(alias);
        if("subject" in log) {
   console.log(prinTaskOnSubject(student.tasks, log["subject"]));
}
else {
	 console.log(prinTasks(student.tasks));
 }
}

if(argv["show"]) {
   student = loadStudent(alias);
   console.log(printMetaInfoStudent(student));
}

if(argv["show-all"]) {
   student = loadStudent(alias);
   console.log(printMetaInfoStudent(student));
   
   console.log(prinTasks(student.tasks));
   
   for(var i = 0; i < student.logs.length;i++) {
       console.log(printLog(student.logs[i])); 
   }
}
 
if(argv["project"]) {
    student = loadStudent(alias);
    
    var log_type;
    if(argv["issue"] || argv["commit"] || argv["command"]) {
        if(argv["issue"]) {
            log_type = "issue";
        } else if(argv["commit"]) {
            log_type = "commit";
        } else {
            log_type = "command";
        }
    }
    else {
        log_type = prompt(GREEN("Är det en kommit, kommentar eller issue? (c/k/i)"));
    }

    var log_result;
    if(argv["bad"]!==0 || argv["good"]!==0) {
        log_result=argv["bad"]*-1+argv["good"];
        DEBUG(log_result);
    }
    else {
        log_result = prompt(GREEN("Är det bra eller dåligt? Minus tal dåligt och positiva tal bra."));
    }
    
    log["type"]=log_type;
    log["result"]=log_result;
	
    student["logs"].push(log);
    student["karma"]+=log_result;
     
    fs.writeFileSync(out_dir+alias+".hjson", hjson.stringify(student), "utf8");
}

INFO(RED("Finish!"));


